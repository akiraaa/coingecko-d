/*
 * coingecko-d
 *  - https://gitlab.com/akiraaa
 *  - 2019, Akiraaa(akimoto akira), LGPL-2.1
 */
module coingecko;

import std.stdio;
import std.conv;
import std.json;
import std.net.curl;
import std.digest.md;

///
/// MD5
///
private string MD5Str(string target){
	ubyte[16] hash_binary = md5Of(target);
	string hash_hexstr = toHexString(hash_binary);
	return hash_hexstr;
}

///
/// Pulbic API: https://www.coingecko.com/api
///
class coinGeckoV3{
	private immutable string baseUrl = "https://api.coingecko.com/api/v3/";
	
	private string request(string req_name){
		return this.baseUrl~req_name;
	}

	bool ping(){
		// 고정 값
		immutable string answer_hash = MD5Str("{\"gecko_says\":\"(V3) To the Moon!\"}");

		// coin gecko에 요청
		auto curl_result = get( request("ping") );
		string json_str = to!string(curl_result);
		string result_hash = MD5Str(json_str);

		if(answer_hash == result_hash){
			return true;
		}
		return false;
	}

	float simplePrice(string ids, string vs_currencies){
		// coin gecko에 요청
		auto curl_result = get( request("simple/price?ids="~ids~"&vs_currencies="~vs_currencies));
		string json_str = to!string(curl_result);
		JSONValue json_obj = parseJSON(json_str);

		// vs_currencies는 소수로 표시
		float vs_currencies_priece = 0.00;
		
		// JSON VALUE 타입 체크하여 해당하는 아이로 변환
		JSONType value_type = json_obj[ids][vs_currencies].type;
		if     (value_type == JSONType.integer ){ // 인티저는 플롯으로 저장
			vs_currencies_priece = to!float(json_obj[ids][vs_currencies].integer);
		}
		else if(value_type == JSONType.float_){
			vs_currencies_priece = json_obj[ids][vs_currencies].floating;
		}
		return vs_currencies_priece;
	}
}

unittest{
	coinGeckoV3 client = new coinGeckoV3();
	assert(client.ping());
	writeln( client.simplePrice("bitcoin", "krw") );
}